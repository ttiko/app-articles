import React, { useEffect, useState } from "react";
import { Article } from "./Article";

export default function App() {
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((data) => setArticles(data));
  };


  const onUpdate = (id, title, body) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        title: title,
        body: body
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then((data) => {
        const updatedArticles = articles.map(article => {
          if (article.id === id) {
            article.title = title;
            article.body = body;
          }

          return article;
        });

        setArticles(article => updatedArticles);
      })
  };

  return (
    <div className="container mt-5">
      <h1>Articles</h1>
      {articles.map(article => (
        <Article
          id={article.id}
          key={article.id}
          title={article.title}
          body={article.body}
          onUpdate={onUpdate}
        />
      ))}
    </div>
  );
}
