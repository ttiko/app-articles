import React, { useState } from "react";

export const Article = ({ title, body, id, onUpdate }) => {
  const [Update, setUpdate] = useState(false);

  const handleUpdate = () => {
    setUpdate(!Update);
  };


  const handleUpdateSubmit = (e) => {
    e.preventDefault();
    onUpdate(id, e.target.title.value, e.target.body.value);
    setUpdate(!Update);
  };

  const handleUpdateClose = (e) => {
    e.preventDefault();
    setUpdate(!Update);
  };

  return (
    <div>
      {Update ? (
        <div className="card mb-5 mt-5">
        <div className="card-body">
          <div className="d-flex flex-row justify-content-between">
            <h4 className="h5">Update</h4>
            <a href="#"
              className="card-link"
              onClick={handleUpdateClose}> Close
            </a>
          </div>
          <form onSubmit={handleUpdateSubmit}>
            <label className="form-label h6">Title</label>
            <input placeholder="Title" name="title" className="form-control" defaultValue={title} />
            <label className="form-label mt-3 h6">Body</label>
            <input placeholder="Body" name="body" className="form-control" defaultValue={body} />
            <button 
              className="btn btn-primary mt-3"
              onSubmit={handleUpdateSubmit}> Save
            </button>
          </form>
          </div>
        </div>  
      ) : (
        <div>
          <div className="card mb-5 mt-5">
            <div className="card-body">
              <h5 className="card-title">{title}</h5>
              <p className="card-text text-muted">{body}</p>
              <button 
                className="btn btn-primary mt-3" 
                onClick={handleUpdate}> Update
              </button>
            </div>
          </div>
        </div>        
      )}
    </div>
  );
};
